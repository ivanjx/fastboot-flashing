﻿using fastboot_flashing.Commons;
using fastboot_flashing.Fastboot;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fastboot_flashing
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Getting device.
                var devices = UsbDevice.ListAsync().Result;

                // Printing found devices.
                Console.WriteLine("Found devices:");

                foreach (var d in devices)
                {
                    Console.WriteLine(
                        "VID: 0x{0:X4} | PID: 0x{1:X4} | RB: {2} B | WB: {3} B",
                        d.VendorId,
                        d.ProductId,
                        d.ReadBufferSize,
                        d.WriteBufferSize);
                }

                // Connecting.
                Console.WriteLine("Connecting to the device...");

                using (UsbDevice device = Core.ConnectToDeviceAsync().Result)
                {
                    // Getting device var.
                    Console.WriteLine("Getting device info...");
                    Console.WriteLine(Core.GetVariableAsync(device, "all").Result);

                    // Prompting src file.
                    Console.Write("File to flash-> ");
                    string srcFile = Console.ReadLine().Replace("\"", string.Empty);

                    // Prompting target partition.
                    Console.Write("Target partition-> ");
                    string targetPartition = Console.ReadLine();

                    // Initialzing progress reporter.
                    Progress progress = new Progress();
                    progress.ProgressChanged += (p) =>
                    {
                        Console.Write("\rFlashing {0:F2}%", p.Current / p.Max * 100);
                    };

                    // Opening file stream.
                    using (Stream fs = File.Open(srcFile, FileMode.Open))
                    {
                        Core.FlashFromStreamAsync(
                            device,
                            fs,
                            fs.Length,
                            targetPartition,
                            progress).Wait();
                        Console.WriteLine();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine();
                Console.WriteLine(ex.ToString());
            }

            // Done.
            Console.Write("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
