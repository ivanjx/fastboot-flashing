﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fastboot_flashing.Commons
{
    public class Progress
    {
        readonly object m_lock = new object();

        public delegate void ProgressChangedEventHandler(Progress sender);
        public event ProgressChangedEventHandler ProgressChanged;

        public double Max
        {
            get;
            private set;
        }

        public double Current
        {
            get;
            private set;
        }

        public void SetProgress(double current, double max)
        {
            Current = current;
            Max = max;

            ProgressChanged?.Invoke(this);
        }

        public void IncreaseCurrentProgress(int by = 1)
        {
            lock (m_lock)
            {
                Current += by;
            }

            ProgressChanged?.Invoke(this);
        }
    }
}
