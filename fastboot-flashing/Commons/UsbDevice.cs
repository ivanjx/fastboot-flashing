﻿using Device.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Usb.Net.Windows;

namespace fastboot_flashing.Commons
{
    /// <summary>
    /// Just a wrapper class in case we will use another library :)
    /// </summary>
    public class UsbDevice : IDisposable
    {
        ConnectedDeviceDefinition m_def;
        WindowsUsbDevice m_device;

        public uint VendorId
        {
            get
            {
                return m_def.VendorId.Value;
            }
        }

        public uint ProductId
        {
            get
            {
                return m_def.ProductId.Value;
            }
        }

        public int WriteBufferSize
        {
            get
            {
                return m_def.WriteBufferSize ?? 0;
            }
        }

        public int ReadBufferSize
        {
            get
            {
                return m_def.ReadBufferSize ?? 0;
            }
        }

        public bool IsOpen
        {
            get
            {
                return m_device != null && m_device.IsInitialized;
            }
        }

        public UsbDevice(ConnectedDeviceDefinition def)
        {
            m_def = def;
        }

        /// <summary>
        /// Opens connection to the device.
        /// </summary>
        public async Task ConnectAsync()
        {
            if (IsOpen)
            {
                throw new Exception("Already connected");
            }

            m_device = DeviceManager.Current.GetDevice(m_def) as WindowsUsbDevice;
            await m_device.InitializeAsync();
        }

        public int Read(byte[] target, int offset, int len)
        {
            return m_device.ReadAsync(target, offset, len).Result;
        }

        public async Task<int> ReadAsync(byte[] buff, int offset, int len)
        {
            return await Task.Run(() => Read(buff, offset, len));
        }

        public void Write(byte[] src, int offset, int len)
        {
            m_device.WriteAsync(src, offset, len).Wait();
        }

        public async Task WriteAsync(byte[] buff, int offset, int len)
        {
            await Task.Run(() => Write(buff, offset, len));
        }

        public void Dispose()
        {
            if (!IsOpen)
            {
                return;
            }

            m_device.Dispose();
        }

        // STATIC METHODS.
        static UsbDevice()
        {
            // Registering device factory.
            WindowsUsbDeviceFactory.Register();
            //WindowsHidDeviceFactory.Register();
        }

        /// <summary>
        /// Lists all devices in current computer.
        /// </summary>
        /// <returns></returns>
        public static async Task<UsbDevice[]> ListAsync()
        {
            return (
                from d in await DeviceManager.Current.GetConnectedDeviceDefinitionsAsync(null)
                select new UsbDevice(d)).ToArray();
        }
    }
}
