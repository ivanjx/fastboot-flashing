﻿using fastboot_flashing.Commons;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace fastboot_flashing.Fastboot
{
    public static class Core
    {
        const int FLASH_BUFF_SIZE = 1024 * 1024;
        static readonly uint[] VENDOR_IDS =
            {
                0x18D1, // Google
                //0x0451, // Texas Instruments
                //0x0502, // Acer
                //0x0FCE, // Sony
                //0x05C6, // Qualcomm
                0x22B8, // Motorola
                //0x0955, // NVIDIA
                //0x413C, // Dell
                //0x2314, // INQ
                //0x0BB4, // HTC
                //0x8087, // Intel
            };

        /// <summary>
        /// Connects to a fastboot port.
        /// </summary>
        /// <returns></returns>
        public static async Task<UsbDevice> ConnectToDeviceAsync()
        {
            // Getting device.
            UsbDevice device = (
                from d in await UsbDevice.ListAsync()
                where VENDOR_IDS.Contains(d.VendorId)
                select d).FirstOrDefault();

            if (device == null)
            {
                throw new Exception("Device not found");
            }

            // Connecting.
            await device.ConnectAsync();

            // Done.
            return device;
        }

        /// <summary>
        /// Sends simple fastboot command.
        /// </summary>
        /// <param name="port"></param>
        /// <param name="command"></param>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static async Task SendCommandAsync(
            UsbDevice device,
            string command,
            string arg = null)
        {
            string data = string.Format(
                "{0}:{1}",
                command,
                arg);
            byte[] dataBuff = Encoding.UTF8.GetBytes(data);
            await device.WriteAsync(dataBuff, 0, dataBuff.Length);
        }

        /// <summary>
        /// Converts bytes to string. Stops when length reached or found NULL char.
        /// </summary>
        /// <param name="buff"></param>
        /// <param name="index"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        static string ReadStringFromBytes(byte[] buff, int index = 0, int length = -1)
        {
            int readLen = length == -1 ? buff.Length - index : length;
            List<byte> resultBuff = new List<byte>();

            for (int i = index; i < readLen; ++i)
            {
                if (buff[i] == 0)
                {
                    break;
                }

                resultBuff.Add(buff[i]);
            }

            return Encoding.UTF8.GetString(resultBuff.ToArray());
        }

        /// <summary>
        /// Reads fastboot device response. This will throw error if device responds with FAIL.
        /// </summary>
        /// <returns></returns>
        public static async Task<(string header, byte[] body)> ReadResponseAsync(UsbDevice device)
        {
            // Reading 64 bytes.
            byte[] readBuff = new byte[64];
            await device.ReadAsync(readBuff, 0, readBuff.Length);

            // Validating header.
            string header = Encoding.UTF8.GetString(readBuff, 0, 4);
            byte[] body;

            if (header == "FAIL")
            {
                throw new Exception(ReadStringFromBytes(readBuff, 4));
            }
            else if (header == "DATA")
            {
                // Just read 8 bytes.
                body = readBuff.Skip(4).Take(8).ToArray();
            }
            else if (header == "OKAY" || header == "INFO")
            {
                // Read the whole body.
                body = readBuff.Skip(4).ToArray();
            }
            else
            {
                // Unknown.
                throw new Exception(string.Format(
                    "Got invalid response {0}: {1}",
                    header,
                    ReadStringFromBytes(readBuff, 4)));
            }

            // Done.
            return (header, body);
        }

        /// <summary>
        /// The 'getvar' helper command.
        /// </summary>
        /// <param name="port"></param>
        /// <param name="variable"></param>
        /// <returns></returns>
        public static async Task<string> GetVariableAsync(UsbDevice device, string variable)
        {
            // Sending command.
            await SendCommandAsync(
                device,
                "getvar",
                variable);

            // Reading response till OKAY.
            StringBuilder sb = new StringBuilder();

            while (true)
            {
                // Reading response.
                var response = await ReadResponseAsync(device);

                if (response.header == "INFO")
                {
                    sb.AppendLine(ReadStringFromBytes(response.body));
                }
                else
                {
                    // Done.
                    break;
                }
            }

            // Done.
            return sb.ToString();
        }

        /// <summary>
        /// Flashes a stream content to the device.
        /// </summary>
        /// <param name="port"></param>
        /// <param name="fs"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public static async Task FlashFromStreamAsync(
            UsbDevice device,
            Stream fs,
            long len,
            string partitionName,
            Progress progress = null,
            CancellationToken cancelToken = default(CancellationToken))
        {
            if (progress == null)
            {
                progress = new Progress();
            }

            // Sending file first with 'download' command.
            await SendCommandAsync(
                device,
                "download",
                string.Format("{0:x8}", len));

            // Reading response.
            var response = await ReadResponseAsync(device);

            if (response.header != "DATA")
            {
                throw new Exception(string.Format(
                    "Invalid response '{0}', expected DATA",
                    response.header));
            }

            long deviceAcceptSize = Convert.ToInt64(Encoding.ASCII.GetString(response.body), 16);

            if (deviceAcceptSize != len)
            {
                // Invalid accept size.
                throw new Exception(string.Format(
                    "Device accepts {0} B instead of {1} B",
                    deviceAcceptSize,
                    len));
            }

            // Flashing file.
            progress.SetProgress(0, len);
            byte[] readBuff = new byte[FLASH_BUFF_SIZE];

            while (progress.Current < len)
            {
                if (cancelToken.IsCancellationRequested)
                {
                    throw new Exception("Operation cancelled");
                }

                int toRead = (int)Math.Min(readBuff.Length, len - progress.Current);
                int read = await fs.ReadAsync(readBuff, 0, toRead);

                if (read == 0)
                {
                    throw new Exception("Unexpected EOF");
                }

                await device.WriteAsync(readBuff, 0, read);
                progress.IncreaseCurrentProgress(read);
            }

            // Reading device response.
            response = await ReadResponseAsync(device);

            if (response.header == "INFO")
            {
                // Getting info string and throw as error.
                throw new Exception(ReadStringFromBytes(response.body));
            }

            // Sending flash command.
            await SendCommandAsync(
                device,
                "flash",
                partitionName);

            // Reading device response.
            response = await ReadResponseAsync(device);

            if (response.header == "INFO")
            {
                // Getting info string and throw as error.
                throw new Exception(ReadStringFromBytes(response.body));
            }
        }

        /// <summary>
        /// Sends a command and gets single response from the device.
        /// </summary>
        /// <param name="device"></param>
        /// <param name="command"></param>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static async Task<string> SendSimpleCommandAsync(
            UsbDevice device,
            string command,
            string arg = null)
        {
            // Sending command.
            await SendCommandAsync(device, command, arg);

            // Getting response.
            var response = await ReadResponseAsync(device);
            return ReadStringFromBytes(response.body);
        }

        /// <summary>
        /// Sends reboot command.
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public static async Task RebootAsync(UsbDevice device)
        {
            await SendSimpleCommandAsync(device, "reboot");
        }

        /// <summary>
        /// Deletes a partition from the device.
        /// </summary>
        /// <param name="device"></param>
        /// <param name="partition"></param>
        /// <returns></returns>
        public static async Task ErasePartitionAsync(UsbDevice device, string partition)
        {
            await SendSimpleCommandAsync(device, "erase", partition);
        }

        /// <summary>
        /// Erases user data on the device.
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public static async Task EraseUserDataAsync(UsbDevice device)
        {
            await ErasePartitionAsync(device, "userdata");
        }
    }
}
